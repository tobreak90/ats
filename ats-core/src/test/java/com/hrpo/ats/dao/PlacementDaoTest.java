package com.hrpo.ats.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.PlacementEvent;

@SpringBootTest
@Transactional
public class PlacementDaoTest extends AbstractWorkflowEventDaoTest {

  @Override
  AbstractWorkflowEvent getWorkflowEvent() {
    return new PlacementEvent();
  }

  @Override
  WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao() {
    return getPlacementDao();
  }
  
  @Test
  void testInsertPlacementEvent() {
    
    testUpsertWorkflowEvent_New();
  }
  
  @Test
  void testDeletePlacementEvent() {
    
    testDeleteByJobOrderCandidateId();
  }
  
  @Test
  void testCountPlacementEventsByUserDateRange_TodayOnly() {
    
    testCountWorkflowEventsByUserDateRange_TodayOnly();
  }
  
  @Test
  void testCountPlacementEventsByUserDateRange_OtherUser() {
    
    testCountWorkflowEventsByUserDateRange_OtherUser();
  }
  
  @Test
  void testCountPlacementEventsByUserDateRange_ThreeDays() {
    
    testCountWorkflowEventsByUserDateRange_ThreeDays();
  }
}
