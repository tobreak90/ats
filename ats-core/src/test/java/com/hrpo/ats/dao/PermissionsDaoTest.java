package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Permission;

@SpringBootTest
@Transactional
public class PermissionsDaoTest {

  @Autowired
  private PermissionDao permissionDao;
  
  @Test
  void testFindByUsernameWithRolePermissions() {

    List<Permission> permissions = permissionDao.findByUsername("dhilario");

    assertNotNull(permissions);
    assertTrue(permissions.containsAll(Arrays.asList(
        new Permission("menu.dashboard"), 
        new Permission("menu.prescreen"),
        new Permission("menu.submission"),
        new Permission("menu.presentation"),
        new Permission("menu.interview"),
        new Permission("menu.placement"))));
  }
}
