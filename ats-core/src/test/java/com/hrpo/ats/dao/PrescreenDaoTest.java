package com.hrpo.ats.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;

@SpringBootTest
@Transactional
public class PrescreenDaoTest extends AbstractWorkflowEventDaoTest {

  @Override
  AbstractWorkflowEvent getWorkflowEvent() {
    return new PrescreenEvent();
  }

  @Override
  WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao() {
    return getPrescreenDao();
  }
  
  @Test
  void testInsertPrescreenEvent() {
    
    testUpsertWorkflowEvent_New();
  }
  
  @Test
  void testDeletePrescreenEvent() {
    
    testDeleteByJobOrderCandidateId();
  }
  
  @Test
  void testCountPrescreenEventsByUserDateRange_TodayOnly() {
    
    testCountWorkflowEventsByUserDateRange_TodayOnly();
  }
  
  @Test
  void testCountPrescreenEventsByUserDateRange_OtherUser() {
    
    testCountWorkflowEventsByUserDateRange_OtherUser();
  }
  
  @Test
  void testCountPrescreenEventsByUserDateRange_ThreeDays() {
    
    testCountWorkflowEventsByUserDateRange_ThreeDays();
  }
}
