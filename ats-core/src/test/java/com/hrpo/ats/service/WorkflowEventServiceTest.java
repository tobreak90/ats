package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.ats.dao.JobOrderCandidateDao;
import com.hrpo.ats.dao.JobOrderDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dto.Candidate;

@SpringBootTest
@Transactional
class WorkflowEventServiceTest {

  @Autowired
  private WorkflowEventService workflowEventService;
  
  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;
  
  @Autowired
  @Qualifier("submissionDao")
  private WorkflowDao<SubmissionEvent> submissionDao;
  
  @Autowired
  @Qualifier("clientSubmissionDao")
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;
  
  @Autowired
  @Qualifier("interviewDao")
  private WorkflowDao<InterviewEvent> interviewDao;
  
  @Autowired
  @Qualifier("placementDao")
  private WorkflowDao<PlacementEvent> placementDao;
  
  private final Candidate candidate = new Candidate();
  private final JobOrder jobOrder1 = new JobOrder();
  private JobOrderCandidate joCandidate1 = null;
  private Integer jobOrderCandidateId = null;
  
  @BeforeEach
  void init(TestInfo testInfo) {
    candidate.setCandidateId("X50");
    candidate.setSource("Dummy Source 1");
    candidate.setFirstName("C1 First");
    candidate.setLastName("C1 Last");
    candidate.setFullName("C1 Fullname");
    candidate.setPhone("+1234567890");
    candidate.setEmail("c1@email.com");
    
    Integer candidateId = candidateDao.upsert(candidate);
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now());
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    
    Integer joId = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    joCandidate.setEventRemarks("remarks");
    
    jobOrderCandidateId = jobOrderCandidateDao.insert(joId, 2, candidateId, joCandidate);
    joCandidate1 = jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
  }

  @Test
  void testSavePrescreenEvent() {
    
    AbstractWorkflowEvent event = new PrescreenEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = workflowEventService.savePrescreenEvent(event, joCandidate1);

    AbstractWorkflowEvent actualEvent = prescreenDao.findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  @Test
  void testSaveSubmissionEvent() {
    
    AbstractWorkflowEvent event = new SubmissionEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = workflowEventService.saveSubmissionEvent(event, joCandidate1);

    AbstractWorkflowEvent actualEvent = submissionDao.findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  @Test
  void testSaveClientSubmissionEvent() {
    
    AbstractWorkflowEvent event = new ClientSubmissionEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = workflowEventService.saveClientSubmissionEvent(event, joCandidate1);

    AbstractWorkflowEvent actualEvent = clientSubmissionDao.findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  @Test
  void testSaveInterviewEvent() {
   
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = workflowEventService.saveInterviewEvent(event, joCandidate1);

    AbstractWorkflowEvent actualEvent = interviewDao.findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  @Test
  void testSavePlacementEvent() {
    
    AbstractWorkflowEvent event = new PlacementEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = workflowEventService.savePlacementEvent(event, joCandidate1);

    AbstractWorkflowEvent actualEvent = placementDao.findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  @Test
  void testSaveSubmissionEvent_WithPredecessorCheck() {
    
    AbstractWorkflowEvent event = new SubmissionEvent();
    event.setTransactionDate(LocalDate.now());
    workflowEventService.saveSubmissionEvent(event, joCandidate1);

    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertEquals(WorkflowStage.SUBMIT, actualJoCandidate.getLastWorkflowStage());
  }
  
  @Test
  void testSaveClientSubmissionEvent_With2PredecessorChecks() {
    
    AbstractWorkflowEvent event = new ClientSubmissionEvent();
    event.setTransactionDate(LocalDate.now());
    workflowEventService.saveClientSubmissionEvent(event, joCandidate1);

    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertTrue(hasSubmissionEvent);
    assertEquals(WorkflowStage.PRESENTATION, actualJoCandidate.getLastWorkflowStage());
  }
  
  @Test
  void testSaveInterviewEvent_With3PredecessorChecks() {
    
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    workflowEventService.saveInterviewEvent(event, joCandidate1);

    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasClientSubmissionEvent = 
        clientSubmissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertTrue(hasSubmissionEvent);
    assertTrue(hasClientSubmissionEvent);
    assertEquals(WorkflowStage.INTERVIEW, actualJoCandidate.getLastWorkflowStage());
  }
  
  @Test
  void testSavePlacementEvent_With4PredecessorChecks() {
    
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    workflowEventService.savePlacementEvent(event, joCandidate1);

    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasClientSubmissionEvent = 
        clientSubmissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasPlacementEvent = 
        placementDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertTrue(hasSubmissionEvent);
    assertTrue(hasClientSubmissionEvent);
    assertTrue(hasPlacementEvent);
    assertEquals(WorkflowStage.PLACEMENT, actualJoCandidate.getLastWorkflowStage());
  }
  
  @Test
  void testDeleteWorkflowEventsByJoCandidateId() {
    
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    workflowEventService.savePlacementEvent(event, joCandidate1);
    
    workflowEventService.deleteWorkflowEventsByJoCandidateId(jobOrderCandidateId);

    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasClientSubmissionEvent = 
        clientSubmissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasPlacementEvent = 
        placementDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    assertFalse(hasPrescreenEvent);
    assertFalse(hasSubmissionEvent);
    assertFalse(hasClientSubmissionEvent);
    assertFalse(hasPlacementEvent);
    assertEquals(null, actualJoCandidate.getLastWorkflowStage());
    assertEquals(null, actualJoCandidate.getEventRemarks());
  }
  
  @Test
  void testFindWorkflowEventsByJoCandidateId() {
    
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    event.setRemarks("test remarks");
    workflowEventService.savePlacementEvent(event, joCandidate1);
    
    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasClientSubmissionEvent = 
        clientSubmissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasPlacementEvent = 
        placementDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    var actualEvents = workflowEventService.getWorkflowEventsByJoCandidateId(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertTrue(hasSubmissionEvent);
    assertTrue(hasClientSubmissionEvent);
    assertTrue(hasPlacementEvent);
    
    assertNotNull(actualEvents);
    assertEquals(5, actualEvents.size());
    assertEquals(WorkflowStage.PLACEMENT, actualJoCandidate.getLastWorkflowStage());
  }
  
  @Test
  void testFindWorkflowEventsByJoCandidateId_4PredecessorChecks() {
    
    AbstractWorkflowEvent event = new InterviewEvent();
    event.setTransactionDate(LocalDate.now());
    event.setRemarks("test remarks");
    workflowEventService.saveInterviewEvent(event, joCandidate1);
    
    Boolean hasPrescreenEvent = 
        prescreenDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasSubmissionEvent = 
        submissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasClientSubmissionEvent = 
        clientSubmissionDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    Boolean hasInterviewEvent = 
        interviewDao.findStateByJobOrderCandidateId(jobOrderCandidateId);
    
    JobOrderCandidate actualJoCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    var actualEvents = workflowEventService.getWorkflowEventsByJoCandidateId(jobOrderCandidateId);
    
    assertTrue(hasPrescreenEvent);
    assertTrue(hasSubmissionEvent);
    assertTrue(hasClientSubmissionEvent);
    assertTrue(hasInterviewEvent);
    
    assertNotNull(actualEvents);
    assertEquals(4, actualEvents.size());
    assertEquals(WorkflowStage.INTERVIEW, actualJoCandidate.getLastWorkflowStage());
  }
}
