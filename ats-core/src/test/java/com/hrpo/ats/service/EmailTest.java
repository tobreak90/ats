package com.hrpo.ats.service;

import java.util.ArrayList;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Team;

@SpringBootTest
@Transactional
@ActiveProfiles("dev")
class EmailTest {
  
  @Autowired
  EmailService email;
  
  @Disabled
  @Test
  public void testEmail() {
    Team team = new Team();
    team.setId(1);
    team.setName("Test Team");
    team.setEmail("test@email.com");
    
    EodReport eodReport = new EodReport();
    eodReport.setEodReportSources(new ArrayList<EodReportSource>());
    email.saveAndSendEmail("dhilario", team, eodReport);
  }
}
