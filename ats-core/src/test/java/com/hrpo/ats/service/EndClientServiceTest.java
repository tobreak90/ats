package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;

@SpringBootTest
@Transactional
public class EndClientServiceTest {
  @Autowired
  private ClientService clientService;
  
  @Autowired
  private EndClientService endClientService;
  
  @Test
  public void testSave_New() {
    EndClient testEndClient = new EndClient();
    
    testEndClient.setName("Test End Client");
    testEndClient.setDescription("Test Description");
    
    Client client = new Client();
    client.setClientId(1);
    testEndClient.setClient(client);
    
    Integer endClientId = endClientService.save(testEndClient);
    
    EndClient actual = endClientService.getEndClientById(endClientId);
    
    assertNotNull(actual);
    assertEquals("Test End Client", actual.getName());
    assertEquals("Client A", actual.getClient().getName());
    assertEquals("Test Description", actual.getDescription());
  }
  
  @Test
  public void testSave_Existing() {
    EndClient testEndClient = new EndClient();
    
    testEndClient.setName("Test End Client");
    testEndClient.setDescription("Test Description");
    
    Client client = new Client();
    client.setClientId(1);
    testEndClient.setClient(client);
    
    Integer endClientId = endClientService.save(testEndClient);
    
    EndClient updateEndClient = endClientService.getEndClientById(endClientId);
    
    updateEndClient.setName("Update End Client");
    updateEndClient.setDescription("Update Description");
    Client testClient = new Client();
    testClient.setClientId(2);
    updateEndClient.setClient(testClient);
    
    endClientService.save(updateEndClient);
    
    EndClient actual = endClientService.getEndClientById(endClientId);
    
    assertNotNull(actual);
    assertEquals("Update End Client", actual.getName());
    assertEquals("Client B", actual.getClient().getName());
    assertEquals("Update Description", actual.getDescription());
  }
  
  @Test
  public void testGetEndClientById() {
    EndClient endClient = endClientService.getEndClientById(1);
    
    assertNotNull(endClient);
    assertEquals("End Client A", endClient.getName());
    assertEquals("Client A", endClient.getClient().getName());
    assertEquals("End Client A Description", endClient.getDescription());
  }
  
  @Test
  public void testGetEndClientsByUser() {
    List<EndClient> endClients = endClientService.getEndClientsByUser("dhilario");
    
    assertNotNull(endClients);
    assertEquals(6, endClients.size());
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client B", endClients.get(1).getName());
  }
  
  @Test
  public void testGetEndClientsByClient() {
    Client client = clientService.getClientById(1, false);
    
    List<EndClient> endClients = endClientService.getEndClientsByClient(client);
    
    assertNotNull(endClients);
    assertEquals(3, endClients.size());
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client A Description", endClients.get(0).getDescription());
    assertEquals("End Client B", endClients.get(1).getName());
    assertEquals("End Client B Description", endClients.get(1).getDescription());
    assertEquals("End Client C", endClients.get(2).getName());
    assertEquals("End Client C Description", endClients.get(2).getDescription());
  }
  
  @Test
  public void testGetAllEndClients() {
    
    List<EndClient> endClients = endClientService.getAllEndClients();
    
    assertNotNull(endClients);
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client A Description", endClients.get(0).getDescription());
    assertEquals("Client A", endClients.get(0).getClient().getName());
    
    assertEquals("End Client B", endClients.get(1).getName());
    assertEquals("End Client B Description", endClients.get(1).getDescription());
    assertEquals("Client A", endClients.get(1).getClient().getName());
    
    assertEquals("End Client C", endClients.get(2).getName());
    assertEquals("End Client C Description", endClients.get(2).getDescription());
    assertEquals("Client A", endClients.get(2).getClient().getName());
  }
}
