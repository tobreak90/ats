$(document).ready(function() {
    var usersTable = $('#users-table').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "searching" : true
    });

    $("#user-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#firstname").val();
        var value2 = $("#lastname").val();
        usersTable.column(2).search(value1, true, false).column(3).search(value2, true, false).draw();
    });

    $("#user-reset").on("click", function(event) {
        event.preventDefault();
        usersTable.column(2).search('', true, false).column(3).search('', true, false).draw();
    });

    var clientsTable = $('#clients-table').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "columnDefs" : [
            {"width" : "10%", "targets" : 2}
        ],
        "searching" : true
    });

    $("#client-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#clientname").val();
        var value2 = $("#clientDescription").val();
        clientsTable.column(0).search(value1, true, false).column(1).search(value2, true, false).draw();
    });

    $("#client-reset").on("click", function(event) {
        event.preventDefault();
        clientsTable.column(0).search('', true, false).column(1).search('', true, false).draw();
    });
});