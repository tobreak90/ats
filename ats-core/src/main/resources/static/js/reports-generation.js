$(document).ready(function() {
    $(".sortable").sortable();
    $(".sortable").disableSelection();

    $(".sortable").hide();
    $(".sortable input").attr({disabled: "disabled"});
    $(".check-" + $("#report-type").val()).removeAttr("disabled");
    $("#cols-" + $("#report-type").val()).show();

    dateFrom = $("#date-from").datepicker().on("change", function() {
        var dateSelected = new Date(Date.parse($(this).val()));

        dateTo.datepicker("option", "minDate", dateSelected);
    });
    dateTo = $("#date-to").datepicker().on("change", function() {
        dateFrom.datepicker("option", "maxDate", new Date(Date.parse($(this).val())));
    });

    $("#report-type").change(function(event) {
        $(".sortable").hide();
        $(".sortable input").attr({disabled: "disabled"});

        $(".check-" + this.value).removeAttr("disabled");
        $("#cols-" + this.value).show();
    });

    $("#reports-form").submit(function(event) {
        var selectedType = $("#report-type").val();
        var isCheckedNone = $(".check-" + selectedType + ":checked").length === 0;

        if (isCheckedNone) {
            alert("Please select at least 1 column.");
            event.preventDefault();
        }
    });
});