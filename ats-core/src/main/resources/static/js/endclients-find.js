$(document).ready(function() {
    var endClientsTable = $('#find-endclients').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "columnDefs" : [
            {"width" : "10%", "targets" : 3}
        ],
        "searching" : true
    });

    $("#endclient-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#endclientname").val();
        var value2 = $("#endclientDescription").val();
        endClientsTable.column(0).search(value1, true, false).column(2).search(value2, true, false).draw();
    });

    $("#endclient-reset").on("click", function(event) {
        event.preventDefault();
        endClientsTable.column(0).search('', true, false).column(2).search('', true, false).draw();
    });
});