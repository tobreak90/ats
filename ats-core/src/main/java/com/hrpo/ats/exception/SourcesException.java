package com.hrpo.ats.exception;

public class SourcesException extends RuntimeException {

  private static final long serialVersionUID = -5865450624472004290L;
  
  public SourcesException(String message, Throwable cause) {
    super(message, cause);
  }
}
