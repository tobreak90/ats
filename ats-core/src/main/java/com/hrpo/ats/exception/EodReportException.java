package com.hrpo.ats.exception;

public class EodReportException extends RuntimeException {

  private static final long serialVersionUID = -653133648208071866L;

  public EodReportException(String message, Throwable cause) {
    super(message, cause);
  }
}
