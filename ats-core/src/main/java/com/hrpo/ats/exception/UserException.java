package com.hrpo.ats.exception;

public class UserException extends RuntimeException {

  private static final long serialVersionUID = 2240520526221798103L;

  public UserException(String message, Throwable cause) {
    super(message, cause);
  }
  
  public UserException(String message) {
    super(message);
  }
}
