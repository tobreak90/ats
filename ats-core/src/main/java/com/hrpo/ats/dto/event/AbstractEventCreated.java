package com.hrpo.ats.dto.event;

import org.springframework.context.ApplicationEvent;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.service.PredecessorVisitor;

public abstract class AbstractEventCreated extends ApplicationEvent {

  private static final long serialVersionUID = 531928974634417422L;
  
  private AbstractWorkflowEvent event;
  private JobOrderCandidate jobOrderCandidate;

  public AbstractEventCreated(Object source, AbstractWorkflowEvent submissionEvent, 
      JobOrderCandidate jobOrderCandidate) {
    
    super(source);
    
    this.event = submissionEvent;
    this.jobOrderCandidate = jobOrderCandidate;
  }

  public AbstractWorkflowEvent getEvent() {
    return event;
  }

  public JobOrderCandidate getJobOrderCandidate() {
    return jobOrderCandidate;
  }
  
  public abstract void accept(PredecessorVisitor visitor);
}
