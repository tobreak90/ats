package com.hrpo.ats.dto;

public class TempJobOrderCandidate {
  
  private Integer usersId;
  private Integer joId;
  private String daxtraCandidateId;
  
  public Integer getUsersId() {
    return usersId;
  }
  
  public void setUsersId(Integer usersId) {
    this.usersId = usersId;
  }
  
  public Integer getJoId() {
    return joId;
  }
  
  public void setJoId(Integer joId) {
    this.joId = joId;
  }
  
  public String getDaxtraCandidateId() {
    return daxtraCandidateId;
  }
  
  public void setDaxtraCandidateId(String daxtraCandidateId) {
    this.daxtraCandidateId = daxtraCandidateId;
  }
}
