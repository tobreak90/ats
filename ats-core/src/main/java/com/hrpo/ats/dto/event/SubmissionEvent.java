package com.hrpo.ats.dto.event;

import java.io.Serializable;

public class SubmissionEvent extends AbstractWorkflowEvent implements Serializable {

  private static final long serialVersionUID = -722109523897248453L;
}