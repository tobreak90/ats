package com.hrpo.ats.dto;

import java.io.Serializable;

public class EndClient implements Serializable {
  
  private static final long serialVersionUID = 5926871832601308768L;
  
  private Integer id;
  private String name;
  private String description;
  private Client client;
  
  public EndClient() {
    super();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }
  
}
