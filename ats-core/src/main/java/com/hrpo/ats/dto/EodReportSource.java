package com.hrpo.ats.dto;

import java.io.Serializable;

public class EodReportSource implements Serializable {

  private static final long serialVersionUID = -6187380604757928469L;
  
  private Integer id;
  private EodReport eodReport;
  private Source source;
  
  private Integer sourceViews;
  private Integer sourceLeads;
  private Integer leads;
  private Integer activity;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public EodReport getEodReport() {
    return eodReport;
  }
  
  public void setEodReport(EodReport eodReport) {
    this.eodReport = eodReport;
  }
  
  public Source getSource() {
    return source;
  }
  
  public void setSource(Source source) {
    this.source = source;
  }
  
  public Integer getSourceViews() {
    return sourceViews;
  }
  
  public void setSourceViews(Integer sourceViews) {
    this.sourceViews = sourceViews;
  }
  
  public Integer getSourceLeads() {
    return sourceLeads;
  }
  
  public void setSourceLeads(Integer sourceLeads) {
    this.sourceLeads = sourceLeads;
  }
  
  public Integer getLeads() {
    return leads;
  }
  
  public void setLeads(Integer leads) {
    this.leads = leads;
  }
  
  public Integer getActivity() {
    return activity;
  }
  
  public void setActivity(Integer activity) {
    this.activity = activity;
  }
}
