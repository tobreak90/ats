package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dto.EodReportSource;

public interface EodReportSourceDao {
  
  /**
   * Finds {@code EodReportSource} by primary key.
   * @param id The id of the {@code EodReportSource}
   * @return the {@code EodReportSource}.
   */
  EodReportSource findById(Integer id);
  
  /**
   * Inserts {@code EodReportSource}.
   * @param eodReportSource The {@code EodReportSource}.
   * @return The id of the {@code EodReportSorce}.
   */
  Integer insert(EodReportSource eodReportSource);
  
  /**
   * Inserts {@code EodReportSource}s by batch.
   * @param eodReportId The id of the eod report.
   * @param eodReportSources The list of {@code EodReportSource}s to insert.
   */
  void batchInsert(Integer eodReportId, List<EodReportSource> eodReportSources);
  
  /**
   * Finds list {@code EodReportSouce} by {@code EodReport} id.
   * @param id The id of the {@code EodReport}.
   * @return The list of {@code EodReportSource}.
   */
  List<EodReportSource> findByEodReportId(Integer id);
  
  /**
   * Finds list {@code EodReportSouce} by date range.
   * @param dateFrom the starting date of date range.
   * @param dateTo the end date of date range.
   * @return The list of {@code EodReportSource}.
   */
  List<EodReportSource> findByDate(LocalDate dateFrom, LocalDate dateTo);
  
  /**
   * Deletes {@code EodReportSource}s by eod report id.
   * @param eodReportId The eod report id.
   */
  void deleteByEodReportId(Integer eodReportId);

}
