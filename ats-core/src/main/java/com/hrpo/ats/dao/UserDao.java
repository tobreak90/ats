package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.User;

public interface UserDao {
  
  /**
   * Inserts user.
   * 
   * @param user The user.
   * 
   * @return The row id of the user.
   */
  Integer insert(User user);
  
  /**
   * Updates user.
   * @param user The user.
   * @return The row id of the user.
   */
  Integer update(User user);
  
  /**
   * Returns row id of the user by username.
   * 
   * @param username the username.
   * 
   * @return The row id of the user.
   */
  Integer findIdByUsername(String username);
  
  /**
   * Returns {@code User} by username.
   * 
   * @param username the username.
   * 
   * @return {@code User} details of the username.
   */
  User findByUsername(String username);
  
  /**
   * Returns {@code User} by user id.
   * 
   * @param id the user id.
   * 
   * @return {@code User} details of the user.
   */
  User findById(Integer id);
  
  /**
   * Finds all {@code User}.
   * 
   * @param isEnabledOnly true if find only enabled users, false to include disabled users
   * @return The list of {@code User}s.
   */
  List<User> findAllUsers(Boolean isEnabledOnly);
  
  /**
   * Finds all enabled {@code User} by team id.
   * @param isEnabledOnly true if find only enabled users, false to include disabled users
   * @param teamId The team id.
   * @return The list of {@code User}s.
   */
  List<User> findAllUsersByTeam(Integer teamId, Boolean isEnabledOnly);
}
