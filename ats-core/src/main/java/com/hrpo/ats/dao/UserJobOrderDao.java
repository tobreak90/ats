package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.UserJobOrder;

public interface UserJobOrderDao {
  
  /**
   * Inserts a {@code UserJobOrder}.
   * @param jobOrderId the id of {@code UserJobOrder}'s job order
   * @param userId the id of the user related to the {@code UserJobOrder}.
   * @param isPrimaryOwner true if the specified user id is the primary owner of the job order. 
   * @return the row id of the recently inserted {@code UserJobOrder}.
   */
  Integer insert(Integer jobOrderId, Integer userId, Boolean isPrimaryOwner);
  
  /**
   * Retrieves a list of {@code UserJobOrder} by job order id.
   * @param jobOrderId id of the job order.
   * @return the list of {@code UserJobOrder}s.
   */
  List<UserJobOrder> findByJobOrderId(Integer jobOrderId);
}
