package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;

@Repository
public class EodReportJdbcDao extends AbstractJdbcDao implements EodReportDao {
  
  Logger log = LoggerFactory.getLogger(EodReportJdbcDao.class);
  
  public EodReportJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public Integer insert(EodReport eodReport, Integer userId, Integer teamId) {
    final String sql = "INSERT INTO eod_report "
        + "(users_id, comment, report_html, team_id) VALUES "
        + "(:users_id, :comment, :report_html, :team_id) ";
    
    Map<String, Object> params = new HashMap<>();
    params.put("users_id", userId);
    params.put("comment", eodReport.getComment());
    params.put("report_html", eodReport.getReportHtml());
    params.put("team_id", teamId);
    
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, new MapSqlParameterSource(params), keyHolder, new String[] {"id"});
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer update(EodReport eodReport, Integer userId) {
    final String sql = "UPDATE eod_report "
        + "SET update_date = NOW(), report_html = :report_html, comment = :comment "
        + "WHERE id = :id AND users_id = :users_id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("id", eodReport.getId());
    params.put("users_id", userId);
    params.put("comment", eodReport.getComment());
    params.put("report_html", eodReport.getReportHtml());
    
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, new MapSqlParameterSource(params), keyHolder, new String[] {"id"});
    
    return keyHolder.getKey().intValue();
  }

  @Override
  public EodReport findByIdAndUserId(Integer id, Integer userId) {
    final String sql = "SELECT er.id AS er_id, report_date, create_date, update_date, "
        + "comment, report_html, "
        + "u.id AS u_id, u.username, u.firstname, u.lastname, "
        + "t.id AS t_id, t.name AS t_name "
        + "FROM eod_report er "
        + "INNER JOIN users u ON er.users_id = u.id "
        + "INNER JOIN team t ON er.team_id = t.id "
        + "WHERE er.id = :id AND u.id = :user_id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("id", id);
    params.put("user_id", userId);
    
    EodReport result = jdbcTemplate.queryForObject(sql, 
        params, (res, rowNum) -> {
          EodReport report = new EodReport();
          
          report.setId(res.getInt("er_id"));
          
          report.setReportDate(res.getDate("report_date").toLocalDate());
          report.setCreateDate(res.getObject("create_date", LocalDateTime.class));
          report.setUpdateDate(res.getObject("update_date", LocalDateTime.class));
          report.setComment(res.getString("comment"));
          
          report.setReportHtml(res.getString("report_html"));
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          
          Team team = new Team();
          team.setId(res.getInt("t_id"));
          team.setName(res.getString("t_name"));
          
          report.setUser(user);
          
          return report;
        });
    
    return result;
  }
  
  @Override
  public List<EodReport> findByReportDate(LocalDate dateFrom, LocalDate dateTo, Integer userId, 
      Integer teamId) {
    String sql = "SELECT er.id AS er_id, report_date, create_date, update_date, "
        + "comment, report_html, "
        + "u.id AS u_id, u.username, u.firstname, u.lastname, "
        + "t.id AS t_id, t.name AS t_name "
        + "FROM eod_report er "
        + "INNER JOIN users u ON er.users_id = u.id "
        + "INNER JOIN team t ON er.team_id = t.id "
        + "WHERE report_date >= :date_from AND report_date <= :date_to ";
        
    
    if (userId != null) {
      sql += "AND u.id = :user_id ";
    }
    
    if (teamId != null) {
      sql += "AND t.id = :team_id ";
    }
    
    sql += "ORDER BY report_date ASC";
    
    Map<String, Object> param = new HashMap<>();
    param.put("date_from", dateFrom);
    param.put("date_to", dateTo);
    
    if (userId != null) {
      param.put("user_id", userId);
    }
    
    if (teamId != null) {
      param.put("team_id", teamId);
    }
    
    List<EodReport> result = jdbcTemplate.query(sql, 
        new MapSqlParameterSource(param), (res, rowNum) -> {
          EodReport report = new EodReport();
          
          report.setId(res.getInt("er_id"));
          
          report.setReportDate(res.getDate("report_date").toLocalDate());
          report.setCreateDate(res.getObject("create_date", LocalDateTime.class));
          report.setUpdateDate(res.getObject("update_date", LocalDateTime.class));
          report.setComment(res.getString("comment"));
          
          report.setReportHtml(res.getString("report_html"));
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          
          Team team = new Team();
          team.setId(res.getInt("t_id"));
          team.setName(res.getString("t_name"));
          
          report.setUser(user);
          
          return report;
        });
    
    return result;
  }

}
