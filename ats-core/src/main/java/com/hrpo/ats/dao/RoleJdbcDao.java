package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Role;

@Repository
public class RoleJdbcDao extends AbstractJdbcDao implements RoleDao  {
  
  public RoleJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public List<Role> findAllRoles() {
    String sql = "SELECT * FROM role";

    List<Role> roles = jdbcTemplate.query(sql, new HashMap<String, String>(),
        (res, rowNum) -> {
          Role role = new Role();
          role.setId(res.getInt("id"));
          role.setName(res.getString("name"));
          role.setDescription(res.getString("description"));
          return role;
        });
    
    return roles;
  }
}
