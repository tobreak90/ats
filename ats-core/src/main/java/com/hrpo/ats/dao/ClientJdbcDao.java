package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;

@Repository
public class ClientJdbcDao extends AbstractJdbcDao implements ClientDao {
  
  Logger log = LoggerFactory.getLogger(ClientJdbcDao.class);

  public ClientJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public Integer insert(Client client) {
    final String sql = "INSERT INTO client (name, description, team_id, is_default) "
        + "VALUES (:name, :description, :team_id, :is_default)";
    
    Map<String, Object> params = new HashMap<>();
    params.put("name", client.getName());
    params.put("description", client.getDescription());
    params.put("team_id", client.getTeam().getId());
    params.put("is_default", client.getIsDefault());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, paramSource, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer update(Client client) {
    final String sql = "UPDATE client SET name = :name, description = :description, "
        + "team_id  = :team_id, is_default = :is_default WHERE id = :id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("id", client.getClientId());
    params.put("name", client.getName());
    params.put("description", client.getDescription());
    params.put("team_id", client.getTeam().getId());
    params.put("is_default", client.getIsDefault());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, paramSource);
    
    return client.getClientId();
  }
  
  @Override
  public Client findById(Integer id) {
    String sql = "SELECT c.id, c.name AS c_name, c.description AS c_description, is_default, "
        + "t.id AS t_id, t.name AS t_name, t.description AS t_description "
        + "FROM client c "
        + "INNER JOIN team t ON c.team_id = t.id "
        + "WHERE c.id = :id";
    
    return this.jdbcTemplate.queryForObject(sql, new MapSqlParameterSource("id", id),
        (res, row) -> {
          Client client = new Client();
          
          client.setClientId(res.getInt("id"));
          client.setName(res.getString("c_name"));
          client.setDescription(res.getString("c_description"));
          client.setIsDefault(res.getBoolean("is_default"));
          
          Team team = new Team();
          team.setId(res.getInt("t_id"));
          team.setName(res.getString("t_name"));
          team.setDescription(res.getString("t_description"));
          
          client.setTeam(team);
          
          return client;
        });
  }
  
  @Override
  public List<Client> findAll() {
    final String sql = "SELECT c.id, c.name AS c_name, c.description AS c_description, is_default, "
        + "t.id AS t_id, t.name AS t_name, t.description AS t_description "
        + "FROM client c "
        + "INNER JOIN team t ON c.team_id = t.id";
    
    List<Client> result = jdbcTemplate.query(sql, (res, row) -> {
      Client client = new Client();
      
      client.setClientId(res.getInt("id"));
      client.setName(res.getString("c_name"));
      client.setDescription(res.getString("c_description"));
      client.setIsDefault(res.getBoolean("is_default"));
      
      Team team = new Team();
      team.setId(res.getInt("t_id"));
      team.setName(res.getString("t_name"));
      team.setDescription(res.getString("t_description"));
      
      client.setTeam(team);
      
      return client;
    });
    
    return result;
  }

  @Override
  public List<Client> findByTeam(Integer teamId) {
    String sql = "SELECT cl.id AS cl_id, cl.name AS cl_name, cl.description AS cl_description, "
        + "cl.is_default, "
        + "tm.id AS tm_id, tm.name AS tm_name, tm.description AS tm_description "
        + "FROM client cl INNER JOIN team tm ON cl.team_id = tm.id "
        + "WHERE cl.team_id = :tm_id "
        + "ORDER BY cl.name ASC";
    
    List<Client> clients = jdbcTemplate.query(sql, new MapSqlParameterSource("tm_id", teamId),
        (res, row) -> {
          Client client = new Client();
          client.setClientId(res.getInt("cl_id"));
          client.setName(res.getString("cl_name"));
          client.setDescription(res.getString("cl_description"));
          client.setIsDefault(res.getBoolean("is_default"));
          
          Team team = new Team();
          team.setId(res.getInt("tm_id"));
          team.setName(res.getString("tm_name"));
          team.setDescription(res.getString("tm_description"));
          
          client.setTeam(team);
          
          return client;
        });
    
    return clients;
  }

}
