package com.hrpo.ats.dao;

import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.event.InterviewEvent;

@Repository("interviewDao")
public class InterviewJdbcDao extends AbstractWorkflowJdbcDao<InterviewEvent> {

  public InterviewJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  String getWorkflowTableName() {
    return "interview_event";
  }

  @Override
  InterviewEvent getEvent() {
    return new InterviewEvent();
  }
}
