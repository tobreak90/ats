package com.hrpo.ats.dao;

import java.util.List;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Permission;

@Repository
public class PermissionsJdbcDao extends AbstractJdbcDao implements PermissionDao {

  Logger log = LoggerFactory.getLogger(PermissionsJdbcDao.class);

  public PermissionsJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override 
  public List<Permission> findByUsername(String username) {
    
    String sql = "SELECT DISTINCT p.name " 
        + "FROM users u INNER JOIN users_role ur ON u.id = ur.users_id "
        + "INNER JOIN role r ON r.id = ur.role_id INNER JOIN role_permission rp "
        + "ON r.id = rp.role_id INNER JOIN permission p on p.id = rp.permission_id "
        + "WHERE u.username = :username";
    
    List<Permission> permissions = this.jdbcTemplate.query(sql, 
        new MapSqlParameterSource("username", username), (res, rowNum) -> {
          
          return new Permission(res.getString("name"));
        });
    
    return permissions;
  }
}
