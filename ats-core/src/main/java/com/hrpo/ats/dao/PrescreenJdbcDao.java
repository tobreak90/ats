package com.hrpo.ats.dao;

import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.event.PrescreenEvent;

@Repository("prescreenDao")
public class PrescreenJdbcDao extends AbstractWorkflowJdbcDao<PrescreenEvent> {

  public PrescreenJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  String getWorkflowTableName() {
    return "prescreen_event";
  }

  @Override
  PrescreenEvent getEvent() {
    return new PrescreenEvent();
  }
}
