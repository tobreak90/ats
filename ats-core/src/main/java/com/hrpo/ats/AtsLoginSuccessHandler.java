package com.hrpo.ats;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.service.ClientService;

@Component
public class AtsLoginSuccessHandler implements AuthenticationSuccessHandler {
  
  Logger log = LoggerFactory.getLogger(AtsLoginSuccessHandler.class);

  @Autowired
  private ClientService clientService;
  
  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {

    HttpSession session = request.getSession(true); // create a new session.
    
    User user = (User) authentication.getPrincipal();
    Team team = user.getTeam();
    List<Client> teamClients = clientService.getClientsByTeam(team);
    Client defaultClient =
        teamClients.stream().filter(client -> client.getIsDefault() == true)
        .findAny().orElse(teamClients.isEmpty() ? null : teamClients.get(0));
    
    session.setAttribute(SessionKey.USER_TEAM.getName(), team);
    session.setAttribute(SessionKey.TEAM_CLIENTS.getName(), teamClients);
    session.setAttribute(SessionKey.SELECTED_CLIENT.getName(), defaultClient);
    session.setAttribute(SessionKey.USER_FULL_NAME.getName(), 
        user.getFirstName() + " " + user.getLastName());
    session.setAttribute(SessionKey.SELECTED_DAXTRA_DB.getName(), DaxtraDatabase.OLD_DAXTRA);
    
    // let's redirect to home page unless there's a requirement to redirect
    // depending on the role/authority of the user.
    response.sendRedirect("/");
  }

}
