package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.form.EndClientForm;

public interface EndClientTransformer extends Transformer<EndClientForm, EndClient> {
  
  public EndClient transform(EndClientForm form);
  
  default EndClient transformToDto(EndClientForm form) {
    EndClient endClient = new EndClient();
    
    endClient.setId(form.getId());
    endClient.setName(form.getName());
    endClient.setDescription(form.getDescription());
    
    Client client = new Client();
    ClientForm clientForm = form.getClient();
    client.setClientId(clientForm.getId());
    client.setName(clientForm.getName());
    client.setDescription(clientForm.getDescription());
    
    endClient.setClient(client);
    
    return endClient;
  }

}
