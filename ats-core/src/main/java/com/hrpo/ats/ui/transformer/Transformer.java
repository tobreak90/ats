package com.hrpo.ats.ui.transformer;

/**
 * This interface will mostly be used as a framework for transforming
 * DTO to HTML form objects and vice-versa.
 */
public interface Transformer<T, R> {

  public R transform(T type);
}
