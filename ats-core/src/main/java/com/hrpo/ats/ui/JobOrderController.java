package com.hrpo.ats.ui;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.SessionKey;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.exception.JobOrderException;
import com.hrpo.ats.service.EndClientService;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.service.UserService;
import com.hrpo.ats.ui.form.JobOrderForm;
import com.hrpo.ats.ui.transformer.JobOrderFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderTransformer;

@Controller
@RequestMapping("joborder")
public class JobOrderController {

  Logger log = LoggerFactory.getLogger(JobOrderController.class);

  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private EndClientService endClientService;
  
  @Autowired
  private JobOrderTransformer jobOrderTransformer;
  
  @Autowired
  private JobOrderFormTransformer jobOrderFormTransformer;
  
  @Autowired
  private UserService userService;

  @InitBinder
  public void dataBinding(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
  }
  
  @RequestMapping(value = "/endclients", 
      method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody List<EndClient> getEndClients(HttpSession session) {
    Client selectedClient = 
        (Client) session.getAttribute(SessionKey.SELECTED_CLIENT.getName());
    
    List<EndClient> availableEndClients = 
        endClientService.getEndClientsByClient(selectedClient);
    
    return availableEndClients;
  }

  @GetMapping("/new")
  public String show(Model model, Principal principal, HttpSession session) {
    
    Client selectedClient = 
        (Client) session.getAttribute(SessionKey.SELECTED_CLIENT.getName());
    log.debug(selectedClient.getName());
    
    List<EndClient> availableEndClients = 
        endClientService.getEndClientsByClient(selectedClient);
    
    model.addAttribute("endClients", availableEndClients);
    model.addAttribute("jobOrderForm", new JobOrderForm());
    
    return "joborder-edit";
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public ModelAndView saveJobOrder(
      @Valid @ModelAttribute(value = "jobOrderForm") JobOrderForm jobOrderForm, 
      BindingResult bindingResult, ModelMap model, HttpSession session, Principal principal) {
    
    log.info("Uploaded file: " + jobOrderForm.getFile().getOriginalFilename() 
        + "; MIME Type: " + jobOrderForm.getFile().getContentType());
    
    Client selectedClient = 
        (Client) session.getAttribute(SessionKey.SELECTED_CLIENT.getName());
    
    if (bindingResult.hasErrors()) {
      List<EndClient> availableEndClients = 
          endClientService.getEndClientsByClient(selectedClient);
      
      model.addAttribute("endClients", availableEndClients);
      return new ModelAndView("joborder-edit");
    }

    JobOrder jobOrder = jobOrderTransformer.transform(jobOrderForm);
    Integer jobOrderId = null;
    
    try {
      jobOrderId = jobOrderService.saveJobOrder(jobOrder, principal.getName());
      jobOrder.setId(jobOrderId);
    } catch (JobOrderException ex) {
      if (ex.getCause() instanceof DuplicateKeyException) {
        List<EndClient> availableEndClients = 
            endClientService.getEndClientsByClient(selectedClient);
        
        model.addAttribute("endClients", availableEndClients);
        model.addAttribute("errorMessage", ex.getMessage());
        return new ModelAndView("joborder-edit");
      }
    }
    
    model.addAttribute("id", jobOrder.getId().get());
    return new ModelAndView("redirect:/joborder/view", model);
  }

  @RequestMapping(value = "/edit", params = {"id"}, method = RequestMethod.GET)
  public String editJobOrder(@RequestParam(value = "id") String id, ModelMap model, 
      Principal principal) {

    JobOrder jobOrder = jobOrderService
        .getJobOrderWithCandidatesById(Integer.valueOf(id), principal.getName());
    
    model.addAttribute("jobOrderForm", jobOrderFormTransformer.transform(jobOrder));
    model.addAttribute("id", jobOrder.getId().get());

    return "joborder-edit";
  }

  @RequestMapping(value = "/view", params = {"id"}, method = RequestMethod.GET)
  public String viewJobOrder(Model model, @RequestParam(value = "id") String id,
      Principal principal) {
    
    String username = null;
    UserDetails user = userService.loadUserByUsername(principal.getName());
    if (!user.getAuthorities().contains(new SimpleGrantedAuthority("app.workflow.edit"))) {
      username = principal.getName();
    }

    JobOrder jobOrder = jobOrderService
        .getJobOrderWithCandidatesById(Integer.valueOf(id), username);
    
    model.addAttribute("jobOrderForm", jobOrderFormTransformer.transform(jobOrder));

    return "joborder-view";
  }

  @RequestMapping(value = "/find", method = RequestMethod.GET)
  public String findJobOrder(ModelMap model, HttpSession session, Principal principal) {
    
    UserDetails user = userService.loadUserByUsername(principal.getName());
    
    if (user.getAuthorities().contains(new SimpleGrantedAuthority("app.workflow.edit"))) {
      getAllJobOrders(null, null, model);
    } else {
      Client client = (Client) session.getAttribute(SessionKey.SELECTED_CLIENT.getName());
      getAllJobOrders(principal.getName(), client, model);
    }
    
    return "joborder-find";
  }

  @RequestMapping(value = "/find", 
      params = {"number", "title"}, method = RequestMethod.GET)
  public String findJobOrder(ModelMap model, 
      @RequestParam(value = "number") String number, @RequestParam(value = "title") String title,
      Principal principal, HttpSession session) {
    
    Client client = (Client) session.getAttribute(SessionKey.SELECTED_CLIENT.getName());
    
    if (number == null && title == null) {
      getAllJobOrders(principal.getName(), client, model);
      return "joborder-find";
    }
    
    List<JobOrder> jobOrders = jobOrderService.searchJobOrderByCriteria(principal.getName(),
        number, title, client);
    
    List<JobOrderForm> jobOrderForms = 
        jobOrders.stream().map(jobOrderFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("jobOrderForms", jobOrderForms);
    
    return "joborder-find";
  }
  
  @RequestMapping(value = "/download", params = {"joid"}, method = RequestMethod.GET)
  public ResponseEntity<Resource> downloadFile(Model model, 
      @RequestParam(value = "joid") String jobOrderId) {
    JobOrder jo = jobOrderService.getJobOrderFileById(Integer.valueOf(jobOrderId));
    
    ByteArrayResource resource = new ByteArrayResource(jo.getFile());
    
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_DISPOSITION, 
        "attachment; filename=\"" + jo.getFileName() + "\""); 
    
    return ResponseEntity.ok().contentLength((long) jo.getFile().length)
        .headers(headers)
        .contentType(MediaType.parseMediaType(jo.getMimeType()))
        .body(resource);
  }
  
  private void getAllJobOrders(String username, Client client, ModelMap model) {
    List<JobOrder> jobOrders = jobOrderService.getAllJobOrdersForUser(username, client);
    model.addAttribute("jobOrderForms", 
        jobOrders.stream().map(jobOrderFormTransformer::transform).collect(Collectors.toList()));
  }
}
