package com.hrpo.ats.ui.form.validation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RUNTIME)
@Target(TYPE)
@Constraint(validatedBy = EqualsValidator.class)
public @interface FieldMatches {
  String message() default "Passwords does not match.";
  String value1();
  String value2();
  
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}
