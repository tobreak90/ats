package com.hrpo.ats.ui.form;

import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class TeamForm {
  private Integer id;
  
  @NotBlank(message = "Team Name cannot be empty.")
  private String name;
  
  @Email(message = "Please enter a valid email address.")
  private String email;
  
  private String description;
  private List<UsersForm> users;
  private List<ClientForm> clients;
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }

  public List<UsersForm> getUsers() {
    return users;
  }

  public void setUsers(List<UsersForm> users) {
    this.users = users;
  }

  public List<ClientForm> getClients() {
    return clients;
  }

  public void setClients(List<ClientForm> clients) {
    this.clients = clients;
  }
}
