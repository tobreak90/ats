package com.hrpo.ats.ui.form;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ClientForm {

  private Integer id;
  
  @NotBlank
  private String name;
  private String description;
  
  @NotNull
  private TeamForm team;
  private Boolean isDefault;
  
  private List<EndClientForm> endClients;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TeamForm getTeam() {
    return team;
  }

  public void setTeam(TeamForm team) {
    this.team = team;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public List<EndClientForm> getEndClients() {
    return endClients;
  }

  public void setEndClients(List<EndClientForm> endClients) {
    this.endClients = endClients;
  }
  
}
