package com.hrpo.ats.ui.form.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.hrpo.ats.ui.form.UsersForm;

/**
 * Class for validation of password and confirm password fields if and only if
 * user id is null (i.e. for creating new user).
 */
@Component("usersFormValidator")
public class UsersFormValidator implements Validator {
  @Override
  public boolean supports(Class<?> clazz) {
    return UsersForm.class.isAssignableFrom(clazz);
  }
  
  @Override
  public void validate(Object target, Errors errors) {
    final UsersForm form = (UsersForm) target;
    
    if (form.getId() == null) {
      ValidationUtils.rejectIfEmpty(errors, "password", "error.forms.password.empty");
      ValidationUtils.rejectIfEmpty(errors, "confirmPassword", 
          "error.forms.confirmpassword.empty");
    }
  }
}
