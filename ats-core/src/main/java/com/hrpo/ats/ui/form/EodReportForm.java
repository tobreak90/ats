package com.hrpo.ats.ui.form;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.hrpo.ats.dto.EodReportSource;

public class EodReportForm {
  
  private Integer id;
  private String userName;
  private String firstName;
  private String lastName;
  
  private LocalDate reportDate;
  private LocalDateTime createDate;
  private LocalDateTime updateDate;
  
  private String comment;
  
  private String reportHtml;
  
  private List<EodReportSource> sources;
  private List<EodReportSource> leads;
  private List<EodReportSource> activity;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getReportDate() {
    return reportDate;
  }

  public void setReportDate(LocalDate reportDate) {
    this.reportDate = reportDate;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  public LocalDateTime getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(LocalDateTime updateDate) {
    this.updateDate = updateDate;
  }
  

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getReportHtml() {
    return reportHtml;
  }

  public void setReportHtml(String reportHtml) {
    this.reportHtml = reportHtml;
  }

  public List<EodReportSource> getSources() {
    return sources;
  }

  public void setSources(List<EodReportSource> sources) {
    this.sources = sources;
  }

  public List<EodReportSource> getLeads() {
    return leads;
  }

  public void setLeads(List<EodReportSource> leads) {
    this.leads = leads;
  }

  public List<EodReportSource> getActivity() {
    return activity;
  }

  public void setActivity(List<EodReportSource> activity) {
    this.activity = activity;
  }

}
