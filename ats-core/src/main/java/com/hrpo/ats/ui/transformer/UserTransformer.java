package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.User;
import com.hrpo.ats.ui.form.UsersForm;
import com.hrpo.ats.util.BCryptPasswordEncoderUtil;

@FunctionalInterface
public interface UserTransformer extends Transformer<UsersForm, User> {
  
  User transform(UsersForm usersForm);
  
  default User transformToDto(UsersForm form) {
    User user = new User();
    
    user.setId(form.getId());
    user.setFirstName(form.getFirstName());
    user.setLastName(form.getLastName());
    user.setEmail(form.getEmail());
    user.setUsername(form.getUsername());
    user.setEnabled(form.getEnabled() != null ? form.getEnabled() : Boolean.FALSE);
    user.setTeam(form.getTeam());
    user.setUserRoles(form.getRoles());
    
    String password = form.getPassword();
    if (password != null && !password.isEmpty()) {
      user.setPassword(BCryptPasswordEncoderUtil.encode(password));
    }
    
    return user;
  }
}
