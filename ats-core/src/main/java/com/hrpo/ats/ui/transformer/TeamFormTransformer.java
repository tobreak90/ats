package com.hrpo.ats.ui.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.form.TeamForm;
import com.hrpo.ats.ui.form.UsersForm;

public interface TeamFormTransformer extends Transformer<Team, TeamForm> {
  public TeamForm transform(Team dto);
  
  default TeamForm transformToForm(Team dto) {
    TeamForm form = new TeamForm();
    
    form.setId(dto.getId());
    form.setName(dto.getName());
    form.setEmail(dto.getEmail());
    form.setDescription(dto.getDescription());
    
    List<UsersForm> usersForms = new ArrayList<>();
    
    if (dto.getUsers() != null) {
      usersForms = dto.getUsers().stream().map(user -> {
        UsersForm usersForm = new UsersForm();
        
        usersForm.setId(user.getId());
        usersForm.setRoles(user.getUserRoles());
        usersForm.setFirstName(user.getFirstName());
        usersForm.setLastName(user.getLastName());
        usersForm.setEmail(user.getEmail());
        usersForm.setUsername(user.getUsername());
        
        return usersForm;
      }).collect(Collectors.toList());
    }
    
    form.setUsers(usersForms);
    
    List<ClientForm> clientForms = new ArrayList<>();
    
    if (dto.getClients() != null) {
      clientForms = dto.getClients().stream().map(client -> {
        ClientForm clientForm = new ClientForm();
        
        clientForm.setId(client.getClientId());
        clientForm.setName(client.getName());
        clientForm.setDescription(client.getDescription());
        clientForm.setIsDefault(client.getIsDefault());
        
        return clientForm;
      }).collect(Collectors.toList());
    }
    
    
    form.setClients(clientForms);
    
    return form;
  }
}
