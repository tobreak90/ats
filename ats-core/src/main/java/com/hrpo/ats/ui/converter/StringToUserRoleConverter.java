package com.hrpo.ats.ui.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.hrpo.ats.dto.Role;
import com.hrpo.ats.dto.UserRole;

@Component
public class StringToUserRoleConverter implements Converter<String, UserRole> {

  @Override
  public UserRole convert(String source) {
    Role role = new Role();
    UserRole userRole = new UserRole();
    
    String[] roleIdAndName = source.split(":");
    
    role.setId(Integer.valueOf(roleIdAndName[0]));
    role.setName(roleIdAndName[1]);
    userRole.setRole(role);
    
    return userRole;
  }
}
