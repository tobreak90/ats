package com.hrpo.ats.ui.form;

import javax.validation.constraints.NotBlank;

public class EndClientForm {
  
  private Integer id;
  
  @NotBlank
  private String name;
  private String description;
  private ClientForm client;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }

  public ClientForm getClient() {
    return client;
  }

  public void setClient(ClientForm client) {
    this.client = client;
  }
}
