package com.hrpo.ats.ui.form;

import java.time.LocalDate;

public class EodReportSourceForm {
  
  private LocalDate reportDate;
  private String recruiter;
  private String source;
  private Integer views;
  private Integer leads;
  private Integer otherLeads;
  private Integer activity;
  private String team;
  private String comment;
  
  public LocalDate getReportDate() {
    return reportDate;
  }
  
  public void setReportDate(LocalDate reportDate) {
    this.reportDate = reportDate;
  }
  
  public String getRecruiter() {
    return recruiter;
  }
  
  public void setRecruiter(String recruiter) {
    this.recruiter = recruiter;
  }
  
  public String getSource() {
    return source;
  }
  
  public void setSource(String source) {
    this.source = source;
  }
  
  public Integer getViews() {
    return views;
  }
  
  public void setViews(Integer views) {
    this.views = views;
  }
  
  public Integer getLeads() {
    return leads;
  }
  
  public Integer getOtherLeads() {
    return otherLeads;
  }

  public void setOtherLeads(Integer otherLeads) {
    this.otherLeads = otherLeads;
  }

  public Integer getActivity() {
    return activity;
  }

  public void setActivity(Integer activity) {
    this.activity = activity;
  }

  public void setLeads(Integer leads) {
    this.leads = leads;
  }
  
  public String getTeam() {
    return team;
  }
  
  public void setTeam(String team) {
    this.team = team;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
  
}
