package com.hrpo.ats.ui.form.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;

public class EqualsValidator implements ConstraintValidator<FieldMatches, Object> {
  String value1;
  String value2;
  String message;
  
  public void initialize(FieldMatches constraintAnnotation) {
    this.value1 = constraintAnnotation.value1();
    this.value2 = constraintAnnotation.value2();
    this.message = constraintAnnotation.message();
  }
  
  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
    Object value1Value = new BeanWrapperImpl(value).getPropertyValue(value1);
    Object value2Value = new BeanWrapperImpl(value).getPropertyValue(value2);
    
    if (value1Value != null) {
      Boolean isValid = false;
      
      if (value1Value instanceof String && value2Value instanceof String) {
        isValid = ((String) value1Value).equals((String) value2Value);
      }
      
      if (isValid == false) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(value1)
        .addConstraintViolation().disableDefaultConstraintViolation();
      }
      
      return isValid;
    } else {
      return value2Value == null;
    }
  }

}
