package com.hrpo.ats.ui;

import java.util.List;

/**
 * Transforms a form to its equivalent DTO.
 * 
 * @author user
 *
 * @param <T> the form
 * @param <R> the DTO
 * 
 * @deprecated will be deleted before version 1.0.
 */
@Deprecated
public abstract class AbstractController<T, R> {

  /**
   * Transforms form to DTO.
   * 
   * @param form the form to transform to DTO.
   * 
   * @return R the DTO.
   * 
   * @deprecated will be deleted before version 1.0.
   */
  @Deprecated
  abstract R transform(T form);

  /**
   * Transforms DTO to form.
   * 
   * @param dto the DTO to transform to form.
   * 
   * @return T the form.
   * 
   * @deprecated will be deleted before version 1.0.
   */
  @Deprecated
  abstract T reverseTransform(R dto);
  
  /**
   * Transforms a list of form to list of DTO.
   * 
   * @param form the list of form to transform to a list of DTO.
   * 
   * @return {@code List} of {@link R} the list of DTO.
   * 
   * @deprecated will be deleted before version 1.0.
   */
  @Deprecated
  abstract List<R> listTransform(List<T> form);
  
  /**
   * Transforms a list of DTO to list of form.
   * 
   * @param dto the list of DTO to transform to a list of form.
   * 
   * @return {@code List} of {@link T}  the list of form.
   * 
   * @deprecated will be deleted before version 1.0.
   */
  @Deprecated
  abstract List<T> reverseListTransform(List<R> dto);
}
