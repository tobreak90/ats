package com.hrpo.ats.ui.form.validation;

import java.time.LocalDate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;

public class RangeValidator implements ConstraintValidator<Range, Object> {
  String from;
  String to;
  String message;
  
  @Override
  public void initialize(Range constraintAnnotation) {
    from = constraintAnnotation.from();
    to = constraintAnnotation.to();
    message = constraintAnnotation.message();
  }
  
  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
    Object fromValue = new BeanWrapperImpl(value).getPropertyValue(from);
    Object toValue = new BeanWrapperImpl(value).getPropertyValue(to);
    
    
    if (fromValue != null) {
      Boolean isValid = false;
      
      if (fromValue instanceof Double && toValue instanceof Double) {
        isValid = ((Double) fromValue).compareTo((Double) toValue) <= 0;
      } else if (fromValue instanceof LocalDate && toValue instanceof LocalDate) {
        isValid = ((LocalDate) fromValue).compareTo((LocalDate) toValue) <= 0;
      }
      
      if (isValid == false) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(from)
        .addConstraintViolation().disableDefaultConstraintViolation();
      }
      
      return isValid;
    } else {
      return toValue == null;
    }
  }
}
