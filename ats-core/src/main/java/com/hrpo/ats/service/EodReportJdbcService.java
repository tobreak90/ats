package com.hrpo.ats.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.EodReportDao;
import com.hrpo.ats.dao.EodReportSourceDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.EodReportException;

@Service
@Transactional
public class EodReportJdbcService implements EodReportService {
  
  @Autowired
  private EodReportDao eodReportDao;
  
  @Autowired
  private EodReportSourceDao eodReportSourceDao;

  @Autowired
  private UserDao userDao;

  @Override
  public Integer save(EodReport eodReport, String username) {
    
    User user = userDao.findByUsername(username);
    Integer eodReportId = null;
    
    try {
      if (eodReport.getId() != null) {
        eodReportSourceDao.deleteByEodReportId(eodReport.getId());
        eodReportId = eodReportDao.update(eodReport, user.getId());
      } else {
        eodReportId = eodReportDao.insert(eodReport, user.getId(), user.getTeam().getId());
      }
    } catch (DuplicateKeyException dke) {
      throw new EodReportException("Report already exists", dke);
    }
    
    eodReportSourceDao.batchInsert(eodReportId, eodReport.getEodReportSources());
  
    return eodReportId;
  }

  @Override
  public EodReport getByIdAndUsername(Integer id, String username) {
    
    Integer userId = userDao.findIdByUsername(username);
    
    try {
      EodReport report = eodReportDao.findByIdAndUserId(id, userId);
      report.setEodReportSources(eodReportSourceDao.findByEodReportId(id));
      return report;
    } catch (EmptyResultDataAccessException erdae) {
      throw new EodReportException("No such eod report for id " + id, erdae);
    }
  }

  @Override
  public List<EodReport> getByReportDate(LocalDate dateFrom, LocalDate dateTo, String username) {
    try {
      User user = null;
      
      if (username != null) {
        user = userDao.findByUsername(username);
      }
      
      List<EodReport> reports = eodReportDao.findByReportDate(dateFrom, dateTo, 
          user != null ? user.getId() : null, 
          user != null ? user.getTeam().getId() : null);
      
      var list = reports.stream().map(report -> {
        report.setEodReportSources(eodReportSourceDao.findByEodReportId(report.getId()));
        return report;
      }).collect(Collectors.toList());
      
      return list;
    } catch (EmptyResultDataAccessException erdae) {
      throw new EodReportException("No report for this date", erdae);
    }
  }
  
  @Override
  public List<EodReportSource> getSourceByReportDate(LocalDate dateFrom, LocalDate dateTo) {
    try {
      List<EodReportSource> eodReportSources = eodReportSourceDao.findByDate(dateFrom, dateTo);
      return eodReportSources;
    } catch (EmptyResultDataAccessException erdae) {
      throw new EodReportException("No report for this date", erdae);
    }
  }
  
}
