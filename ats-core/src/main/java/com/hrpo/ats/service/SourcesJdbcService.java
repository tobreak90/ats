package com.hrpo.ats.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.SourcesDao;
import com.hrpo.ats.dto.Source;
import com.hrpo.ats.exception.SourcesException;


@Service
@Transactional
public class SourcesJdbcService implements SourcesService {

  @Autowired
  private SourcesDao sourcesDao;
  
  @Override
  public Integer save(Source source) {
    if (source.getId() == null) {
      return sourcesDao.insert(source);
    } else {
      return sourcesDao.update(source);
    }
  }

  @Override
  public Source getSourceById(Integer id) {
    try {
      return sourcesDao.findById(id);
    } catch (EmptyResultDataAccessException erdae) {
      throw new SourcesException("No such Source with id " + id + "found.", erdae);
    }
  }

  @Override
  public List<Source> getAllSources() {
    return sourcesDao.findAll();
  }

}
