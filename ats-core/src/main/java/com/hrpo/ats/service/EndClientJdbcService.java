package com.hrpo.ats.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.EndClientDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.EndClientException;

@Service
@Transactional
public class EndClientJdbcService implements EndClientService {
  
  @Autowired
  private EndClientDao endClientDao;
  
  @Autowired
  private UserDao userDao;
  
  @Override
  public Integer save(EndClient endClient) {
    if (endClient.getId() == null) {
      return endClientDao.insert(endClient);
    } else {
      return endClientDao.update(endClient);
    }
  }

  @Override
  public EndClient getEndClientById(Integer id) {
    try {
      return endClientDao.findById(id);
    } catch (EmptyResultDataAccessException erdae) {
      throw new EndClientException("No such end client", erdae);
    }
  }

  @Override
  public List<EndClient> getEndClientsByUser(String username) {
    try {
      User user = userDao.findByUsername(username);
      return endClientDao.findEndClientsByUserId(user.getId());
    } catch (EmptyResultDataAccessException erdae) {
      throw new EndClientException("No such end client", erdae);
    }
  }
  
  @Override
  public List<EndClient> getEndClientsByClient(Client client) {
    try {
      return endClientDao.findEndClientsByClient(client.getClientId());
    } catch (EmptyResultDataAccessException erdae) {
      throw new EndClientException("No such end client", erdae);
    }
  }
  
  @Override
  public List<EndClient> getAllEndClients() {
    return endClientDao.findAll();
  }
}
