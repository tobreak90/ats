package com.hrpo.ats.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.JobOrderCandidateDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractEventCreated;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEventCreated;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.InterviewEventCreated;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PlacementEventCreated;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.ats.dto.event.SubmissionEventCreated;

@Service
@Transactional
public class WorkflowEventJdbcService implements WorkflowEventService, 
    ApplicationListener<AbstractEventCreated>, PredecessorVisitor {

  Logger log = LoggerFactory.getLogger(WorkflowEventJdbcService.class);
  
  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;
  
  @Autowired
  @Qualifier("submissionDao")
  private WorkflowDao<SubmissionEvent> submissionDao;
  
  @Autowired
  @Qualifier("clientSubmissionDao")
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;
  
  @Autowired
  @Qualifier("interviewDao")
  private WorkflowDao<InterviewEvent> interviewDao;
  
  @Autowired
  @Qualifier("placementDao")
  private WorkflowDao<PlacementEvent> placementDao;
  
  @Autowired
  private ApplicationEventPublisher eventPublisher;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Override
  public Integer savePrescreenEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate) {
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
    return prescreenDao.upsert(event, joCandidate.getId());
  }

  @Override
  public Integer saveSubmissionEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate) {
    
    Integer rowId = submissionDao.upsert(event, joCandidate.getId());
    
    //Set remarks to null so that it will only appear on this workflow
    event.setRemarks(null);
    eventPublisher.publishEvent(
        new SubmissionEventCreated(this, event, joCandidate));
    
    return rowId;
  }

  @Override
  public Integer saveClientSubmissionEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate) {
    
    Integer rowId = clientSubmissionDao.upsert(event, joCandidate.getId());
    
    //Set remarks to null so that it will only appear on this workflow
    event.setRemarks(null);
    eventPublisher.publishEvent(
        new ClientSubmissionEventCreated(this, event, joCandidate));
    
    return rowId;
  }

  @Override
  public Integer saveInterviewEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate) {
    
    Integer rowId = interviewDao.upsert(event, joCandidate.getId());
    
    //Set remarks to null so that it will only appear on this workflow
    event.setRemarks(null);
    eventPublisher.publishEvent(
        new InterviewEventCreated(this, event, joCandidate));
    
    return rowId;
  }

  @Override
  public Integer savePlacementEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate) {
    
    Integer rowId = placementDao.upsert(event, joCandidate.getId());
    //Set remarks to null so that it will only appear on this workflow
    event.setRemarks(null);    
    eventPublisher.publishEvent(
        new PlacementEventCreated(this, event, joCandidate));
    
    return rowId;
  }
  
  @Override
  public void deleteWorkflowEventsByJoCandidateId(Integer joCandidateId) {
    prescreenDao.deleteByJobOrderCandidateId(joCandidateId);
    submissionDao.deleteByJobOrderCandidateId(joCandidateId);
    clientSubmissionDao.deleteByJobOrderCandidateId(joCandidateId);
    interviewDao.deleteByJobOrderCandidateId(joCandidateId);
    placementDao.deleteByJobOrderCandidateId(joCandidateId);
    
    var jobOrderCandidate = new JobOrderCandidate();
    jobOrderCandidate.setId(joCandidateId);
    
    jobOrderCandidateDao.updateWorkflowStage(jobOrderCandidate, null);
  }
  
  @Override
  public List<AbstractWorkflowEvent> getWorkflowEventsByJoCandidateId(Integer joCandidateId) {
    AbstractWorkflowEvent[] events = {
      prescreenDao.findByJobOrderCandidateId(joCandidateId),
      submissionDao.findByJobOrderCandidateId(joCandidateId),
      clientSubmissionDao.findByJobOrderCandidateId(joCandidateId),
      interviewDao.findByJobOrderCandidateId(joCandidateId),
      placementDao.findByJobOrderCandidateId(joCandidateId),
    };
    
    return Arrays.asList(events).stream().filter(event ->  event != null)
        .collect(Collectors.toList());
  }
  
  @Override
  public void onApplicationEvent(AbstractEventCreated event) {
    event.accept(this);
  }
  
  @Override
  public void visit(SubmissionEventCreated event) {
    // Check if predecessor prescreen exist with same job order candidate id.
    // if not then automatically execute prescreen event. @see issue #48.
    if (!prescreenDao.findStateByJobOrderCandidateId(
        event.getJobOrderCandidate().getId())) {
      
      savePrescreenEvent(event.getEvent(), event.getJobOrderCandidate());
    }
    
    jobOrderCandidateDao.updateWorkflowStage(event.getJobOrderCandidate(), WorkflowStage.SUBMIT);
  }

  @Override
  public void visit(ClientSubmissionEventCreated event) {
    // Check if predecessor submission exist with same job order candidate id.
    // if not then automatically execute client submission event. @see issue #48.
    // This will cause a chain reaction of predecessors.
    if (!submissionDao.findStateByJobOrderCandidateId(
        event.getJobOrderCandidate().getId())) {
      
      saveSubmissionEvent(event.getEvent(), event.getJobOrderCandidate());
    }
    
    jobOrderCandidateDao.updateWorkflowStage(
        event.getJobOrderCandidate(), WorkflowStage.PRESENTATION);
  }

  @Override
  public void visit(InterviewEventCreated event) {
    // Check if predecessor client submission exist with same job order candidate id.
    // if not then automatically execute interview event. @see issue #48.
    // This will cause a chain reaction of predecessors.
    if (!clientSubmissionDao.findStateByJobOrderCandidateId(
        event.getJobOrderCandidate().getId())) {
      
      saveClientSubmissionEvent(event.getEvent(), event.getJobOrderCandidate());
    }
    
    jobOrderCandidateDao.updateWorkflowStage(
        event.getJobOrderCandidate(), WorkflowStage.INTERVIEW);
  }

  @Override
  public void visit(PlacementEventCreated event) {
    // Check if predecessor interview exist with same job order candidate id.
    // if not then automatically execute placement event. @see issue #48.
    // This will cause a chain reaction of predecessors.
    if (!interviewDao.findStateByJobOrderCandidateId(
        event.getJobOrderCandidate().getId())) {
      
      saveInterviewEvent(event.getEvent(), event.getJobOrderCandidate());
    }
    
    jobOrderCandidateDao.updateWorkflowStage(
        event.getJobOrderCandidate(), WorkflowStage.PLACEMENT);
  }
}
