package com.hrpo.ats.service;

import java.util.Map;

public interface EodReportEmailTemplate {
  /**
   * Renders the email body in HTML format.
   * @return the email body in HTML format.
   */
  public String render(Map<String, Object> context);
}
