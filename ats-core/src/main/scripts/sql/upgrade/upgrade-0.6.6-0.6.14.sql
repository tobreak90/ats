ALTER TABLE candidate RENAME COLUMN contact_mobile TO contact_phone;
ALTER TABLE candidate ADD COLUMN contact_mobile VARCHAR(20);