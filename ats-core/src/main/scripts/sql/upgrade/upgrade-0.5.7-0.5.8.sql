DROP TABLE IF EXISTS eod_report_source;
DROP TABLE IF EXISTS eod_report;

CREATE TABLE eod_report (
	id SERIAL,
	users_id INTEGER NOT NULL,
	report_date DATE DEFAULT now() NOT NULL,
	create_date TIMESTAMP DEFAULT now() NOT NULL,
	update_date TIMESTAMP DEFAULT now() NOT NULL,
	report_html TEXT,
	PRIMARY KEY (id),
	CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	CONSTRAINT report_date_unq
		UNIQUE (report_date)
);

CREATE INDEX eod_report_users_id_fk_idx ON eod_report (users_id);
ALTER SEQUENCE eod_report_id_seq RESTART WITH 1000;

CREATE TABLE eod_report_source (
	id SERIAL,
	eod_report_id INTEGER NOT NULL,
	sources_id INTEGER NOT NULL,
	source_views INTEGER,
	source_leads INTEGER,
	leads INTEGER,
	activity INTEGER,
	PRIMARY KEY (id),
	CONSTRAINT eod_report_id_fk
		FOREIGN KEY (eod_report_id)
		REFERENCES eod_report(id),
	CONSTRAINT sources_id_fk
		FOREIGN KEY (sources_id)
		REFERENCES sources(id)
);

CREATE INDEX eod_report_source_eod_report_id_fk_idx ON eod_report_source (eod_report_id);
CREATE INDEX eod_report_source_sources_id_fk_idx ON eod_report_source (sources_id);
ALTER SEQUENCE eod_report_source_id_seq RESTART WITH 1000;