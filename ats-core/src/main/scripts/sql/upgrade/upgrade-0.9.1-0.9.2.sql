BEGIN TRANSACTION;

ALTER TABLE eod_report ADD COLUMN team_id INTEGER;
UPDATE eod_report SET team_id = 
(SELECT u.team_id FROM eod_report er INNER JOIN users u ON u.id = users_id WHERE er.id = eod_report.id);
ALTER TABLE eod_report ALTER COLUMN team_id SET NOT NULL;
ALTER TABLE eod_report ADD CONSTRAINT teams_id_fk FOREIGN KEY (team_id) REFERENCES team(id);
ALTER TABLE eod_report DROP CONSTRAINT users_id_report_date_unq;
ALTER TABLE eod_report ADD CONSTRAINT users_id_team_id_report_date_unq UNIQUE (users_id,team_id,report_date);

CREATE INDEX eod_report_team_id_fk_idx ON eod_report (team_id);

COMMIT;