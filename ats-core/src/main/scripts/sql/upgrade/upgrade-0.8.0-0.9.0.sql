ALTER TABLE job_order_candidate ADD COLUMN is_job_offer BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE job_order_candidate ADD COLUMN job_offer_date DATE;