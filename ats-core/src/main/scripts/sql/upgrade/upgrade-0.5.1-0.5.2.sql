CREATE TABLE sources (
	id SERIAL,
	name VARCHAR(160) NOT NULL,
	description VARCHAR(160),
	index INTEGER NOT NULL,
	PRIMARY KEY (id)
);

ALTER SEQUENCE sources_id_seq RESTART WITH 1000;

INSERT INTO permission (id, name, description) VALUES (22, 'app.sources.edit', 'Sources Edit Permission');
INSERT INTO permission (id, name, description) VALUES (23, 'menu.sources', 'Sources Permission');

INSERT INTO role_permission (role_id, permission_id) VALUES (1, 22);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 23);