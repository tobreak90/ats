ALTER TABLE job_order_candidate ADD COLUMN pay_rate_start DOUBLE PRECISION DEFAULT 0;
ALTER TABLE job_order_candidate ALTER COLUMN pay_rate_start SET NOT NULL;

ALTER TABLE job_order_candidate ADD COLUMN pay_rate_end DOUBLE PRECISION DEFAULT 0;
ALTER TABLE job_order_candidate ALTER COLUMN pay_rate_end SET NOT NULL;

ALTER TABLE job_order_candidate ADD COLUMN pay_period_rate rate DEFAULT 'perhour';
ALTER TABLE job_order_candidate ALTER COLUMN pay_period_rate SET NOT NULL;