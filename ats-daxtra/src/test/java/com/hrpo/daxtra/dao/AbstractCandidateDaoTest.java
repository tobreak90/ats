package com.hrpo.daxtra.dao;

import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.mockito.InjectMocks;
import com.hrpo.daxtra.model.response.Contact;
import com.hrpo.daxtra.model.response.Result;
import com.hrpo.daxtra.model.response.StructuredOptions;
import com.hrpo.daxtra.model.response.User;

/**
 * An abstract test class that encapsulates reusable {@code Candidate} specific information.
 */
abstract class AbstractCandidateDaoTest extends AbstractDaoTest {
  
  @InjectMocks
  private DaxtraCandidateRestDao candidateDao;

  private final StructuredOptions structuredOptions1 = new StructuredOptions();
  
  private final Result candidateResult1 = new Result();
  
  public AbstractCandidateDaoTest() {
    super();
  }
  
  @BeforeEach
  void init(TestInfo testInfo) {
    Contact contact1 = new Contact();
    contact1.setPhone("+630123456789");
    contact1.setMobile("+630123456789");
    contact1.setEmail("test@gmail.com");
    Optional<User> user = Optional.of(new User());
    user.get().setStr2(Optional.of("Dummy Source"));
    structuredOptions1.setFirstName("Dummy FirstName");
    structuredOptions1.setLastName("Dummy LastName");
    structuredOptions1.setFullName("Dummy FullName");
    structuredOptions1.setContact(Optional.of(contact1));
    structuredOptions1.setUser(user);
    
    candidateResult1.setCandidateId("DUMMY-CANDIDATE-ID");
    candidateResult1.setFirstName("Dummy FirstName");
    candidateResult1.setLastName("Dummy LastName");
    candidateResult1.setContact(Optional.of(contact1));
    candidateResult1.setUser(user);
  }

  protected DaxtraCandidateRestDao getCandidateDao() {
    return candidateDao;
  }

  protected StructuredOptions getStructuredOptions1TestData() {
    return structuredOptions1;
  }
  
  protected Result getCandidateResult1() {
    return candidateResult1;
  }
  
  
}
