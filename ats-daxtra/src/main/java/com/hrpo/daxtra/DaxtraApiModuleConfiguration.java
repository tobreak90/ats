package com.hrpo.daxtra;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.hrpo.daxtra.dao.DaxtraCandidateDao;
import com.hrpo.daxtra.dao.DaxtraCandidateRestDao;

@Configuration
public class DaxtraApiModuleConfiguration {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
  
  @Bean
  public DaxtraCandidateDao candidateRestDao() {
    return new DaxtraCandidateRestDao();
  }
}
