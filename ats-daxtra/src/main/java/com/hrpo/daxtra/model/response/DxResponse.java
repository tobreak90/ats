package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DxResponse implements Serializable {

  private static final long serialVersionUID = 1398297116558225313L;

  @JsonProperty("Status")
  private Status status;
  @JsonProperty("Results")
  private Results results;
  @JsonProperty("Candidate")
  private CandidateResponse candidate;

  public DxResponse() {
    super();
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Results getResults() {
    return results;
  }

  public void setResults(Results results) {
    this.results = results;
  }

  public CandidateResponse getCandidate() {
    return candidate;
  }

  public void setCandidate(CandidateResponse candidate) {
    this.candidate = candidate;
  }

  @Override
  public String toString() {
    return "DxResponse [status=" + status + ", results=" + results + ", candidate=" + candidate
        + "]";
  }
}
