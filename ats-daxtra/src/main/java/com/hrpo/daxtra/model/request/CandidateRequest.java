package com.hrpo.daxtra.model.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hrpo.daxtra.model.response.StructuredOptions;

@JsonInclude(Include.NON_NULL)
public class CandidateRequest implements Serializable {

  private static final long serialVersionUID = 2619762291759484857L;
  @JsonProperty("CandidateId")
  private String candidateId;
  @JsonProperty("Tags")
  private Tags tags;
  @JsonProperty("StructuredOptions")
  private StructuredOptions structuredOptions;

  public CandidateRequest() {
    super();
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }

  public Tags getTags() {
    return tags;
  }

  public void setTags(Tags tags) {
    this.tags = tags;
  }
  
  public StructuredOptions getStructuredOptions() {
    return structuredOptions;
  }

  public void setStructuredOptions(StructuredOptions structuredOptions) {
    this.structuredOptions = structuredOptions;
  }

  @Override
  public String toString() {
    return "Candidate [tags=" + tags + "]";
  }

}
