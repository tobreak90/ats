package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

@JsonInclude(Include.NON_EMPTY)
public class CandidateId implements Serializable {

  private static final long serialVersionUID = 1075131125247379863L;
  
  @JacksonXmlProperty(localName = "condition", isAttribute = true)
  private String condition;
  
  @JacksonXmlText
  private String value;

  public String getCondition() {
    return condition;
  }

  public void setCondition(String condition) {
    this.condition = condition;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
