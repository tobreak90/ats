package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import java.util.Optional;
import java.util.StringJoiner;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Result implements Serializable {
  
  private static final long serialVersionUID = -7814897686980907491L;
  
  @JsonProperty("CandidateId")
  private String candidateId;
  @JsonProperty("Score")
  private Double score;
  
  //Fields for multiple candidate response
  @JsonProperty("FirstName")
  private String firstName;
  @JsonProperty("LastName")
  private String lastName;
  @JsonProperty("Contact")
  private Optional<Contact> contact = Optional.empty();
  @JsonProperty("User")
  private Optional<User> user = Optional.empty();
  
  public Result() {
    super();
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }

  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Optional<Contact> getContact() {
    return contact;
  }

  public void setContact(Optional<Contact> contact) {
    this.contact = contact;
  }

  public Optional<User> getUser() {
    return user;
  }

  public void setUser(Optional<User> user) {
    this.user = user;
  }
  
  @Override
  public String toString() {
    StringJoiner resultString = new StringJoiner(", ", "[", "]");
    resultString.add("candidateId=" + candidateId).add("score=" + score);
    if (firstName != null || lastName != null) {
      resultString.add("firstName=" + firstName)
        .add("lastName=" + lastName);
    }
    
    return "Result " + resultString;
  }
  
}
