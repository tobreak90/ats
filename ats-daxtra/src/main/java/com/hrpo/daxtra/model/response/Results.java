package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class Results implements Serializable {

  private static final long serialVersionUID = -6869358021305192500L;
  
  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("Result")
  private List<Result> results;

  public Results() {
    super();
  }
  
  public Results(String results) {
    super();
    
    if (results.trim().replaceAll("\n", "").isEmpty()) {
      this.results = new ArrayList<>();
    }
  }

  public List<Result> getResultList() {
    return results;
  }

  public void setResultList(List<Result> tags) {
    this.results = tags;
  }

  @Override
  public String toString() {
    return "Results [results=" + results + "]";
  }
  
}
