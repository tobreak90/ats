package com.hrpo.daxtra.model.request;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class Tags implements Serializable {

  private static final long serialVersionUID = 6582220616020527774L;
  
  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("Tag")
  private List<Tag> tags;

  public Tags() {
    super();
  }

  public List<Tag> getTags() {
    return tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return "Tags [tags=" + tags + "]";
  }
 
}
