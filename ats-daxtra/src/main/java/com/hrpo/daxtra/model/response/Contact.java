package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Contact implements Serializable {
  
  private static final long serialVersionUID = 6157445484113899882L;
  
  @JsonProperty("Phone")
  private String phone;
  @JsonProperty("Mobile")
  private String mobile;
  @JsonProperty("Email")
  private String email;
  
  public Contact() {
    super();
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "Contact [phone=" + phone + ", email=" + email + "]";
  }
}
