package com.hrpo.daxtra.dao;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.hrpo.daxtra.model.request.DxRequest;
import com.hrpo.daxtra.model.response.DxResponse;

public abstract class AbstractDaxtraRestDao {

  private static final String USERNAME = "24h_hr_process";
  private static final String PASSWORD = "24h-hr-process1!";
  private static final String DATABASE = "ws_24hrpo_dev";
  
  protected String daxtraEndpointUri = "https://es-tok-101.daxtra.com/ws/dispatch";

  @Autowired
  private RestTemplate restTemplate;

  private HttpHeaders requestHeaders;
  private DxRequest dxRequest;

  public AbstractDaxtraRestDao() {
    super();

    requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.TEXT_XML);
    requestHeaders.setAccept(Arrays.asList(MediaType.TEXT_XML));

    dxRequest = new DxRequest();
    dxRequest.setUsername(USERNAME);
    dxRequest.setPassword(PASSWORD);
    dxRequest.setDatabase(DATABASE);
  }
  
  
  protected ResponseEntity<DxResponse> sendExchange(String database) {
    System.out.println("send exchange with database");
    dxRequest.setDatabase(database);
    return restTemplate.exchange(
        daxtraEndpointUri,
        HttpMethod.POST,
        new HttpEntity<>(dxRequest, requestHeaders),
        DxResponse.class
        );
  }
  
  protected DxRequest getDxRequest() {
    return dxRequest;
  }
}
