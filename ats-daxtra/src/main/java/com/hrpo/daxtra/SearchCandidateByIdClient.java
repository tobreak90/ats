package com.hrpo.daxtra;

import java.util.Arrays;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.hrpo.daxtra.model.request.CandidateRequest;
import com.hrpo.daxtra.model.request.DxRequest;
import com.hrpo.daxtra.model.response.CandidateId;
import com.hrpo.daxtra.model.response.DxResponse;
import com.hrpo.daxtra.model.response.StructuredOptions;

public class SearchCandidateByIdClient {

  public static void main(String[] args) throws JsonProcessingException {
    final String uri = "https://es-tok-101.daxtra.com/ws/dispatch";

    final RestTemplate restTemplate = new RestTemplate();

    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.TEXT_XML);
    requestHeaders.setAccept(Arrays.asList(MediaType.TEXT_XML));

    DxRequest dxRequest = new DxRequest();
    dxRequest.setAction("search_candidates");
    dxRequest.setOptions("details");
    dxRequest.setUsername("24h_hr_process");
    dxRequest.setPassword("24h-hr-process1!");
    dxRequest.setDatabase("ws_24hrpo_dev");

    final CandidateRequest candidate = new CandidateRequest();
    StructuredOptions options = new StructuredOptions();
    CandidateId canId = new CandidateId();
    
    canId.setCondition("in");
    canId.setValue("XC562065|XC494947|XC368568");
    options.setCandidateId(canId);
    candidate.setStructuredOptions(options);
    
    dxRequest.setCandidate(candidate);
    
    System.out.println(dxRequest);

    HttpEntity<DxRequest> request = new HttpEntity<>(dxRequest, requestHeaders);
    
    ResponseEntity<DxResponse> response = restTemplate.exchange(
        uri,
        HttpMethod.POST,
        request,
        DxResponse.class
        );

    System.out.println(response);

    System.out.println(response.getHeaders());
    System.out.println(response.getStatusCode());
    System.out.println(response.getStatusCodeValue());
    System.out.println("Result: " + response.getBody().getResults());
    System.out.println("Status: " + response.getBody().getStatus());

  }

}
