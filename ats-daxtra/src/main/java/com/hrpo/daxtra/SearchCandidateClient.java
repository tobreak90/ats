package com.hrpo.daxtra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.hrpo.daxtra.model.request.CandidateRequest;
import com.hrpo.daxtra.model.request.DxRequest;
import com.hrpo.daxtra.model.request.Tag;
import com.hrpo.daxtra.model.request.Tags;
import com.hrpo.daxtra.model.response.DxResponse;

public class SearchCandidateClient {

  public static void main(String[] args) {

    final String uri = "https://es-tok-101.daxtra.com/ws/dispatch";

    final RestTemplate restTemplate = new RestTemplate();

    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.TEXT_XML);
    requestHeaders.setAccept(Arrays.asList(MediaType.TEXT_XML));

    DxRequest dxRequest = new DxRequest();
    dxRequest.setAction("search_candidates");
    dxRequest.setUsername("24h_hr_process");
    dxRequest.setPassword("24h-hr-process1!");
    dxRequest.setDatabase("ws_24hrpo_dev");

    final CandidateRequest candidate = new CandidateRequest();
    final Tags tags = new Tags();
    Tag tag = new Tag();
    tag.setName("test MRE");
    tag.setType("SLST");
    List<Tag> tagList = new ArrayList<>();
    tagList.add(tag);
    tags.setTags(tagList);

    candidate.setTags(tags);
    dxRequest.setCandidate(candidate);
    
    System.out.println(dxRequest);

    HttpEntity<DxRequest> request = new HttpEntity<>(dxRequest, requestHeaders);

    ResponseEntity<DxResponse> response = restTemplate.exchange(
        uri,
        HttpMethod.POST,
        request,
        DxResponse.class
        );

    System.out.println(response);

    System.out.println(response.getHeaders());
    System.out.println(response.getStatusCode());
    System.out.println(response.getStatusCodeValue());
    System.out.println("Result: " + response.getBody().getResults());
    System.out.println("Status: " + response.getBody().getStatus());
  }

}
